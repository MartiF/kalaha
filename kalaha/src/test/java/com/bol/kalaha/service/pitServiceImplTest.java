package com.bol.kalaha.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bol.kalaha.dao.PitDao;
import com.bol.kalaha.enums.Player;
import com.bol.kalaha.exception.exceptions.PitNotFoundException;
import com.bol.kalaha.model.Game;
import com.bol.kalaha.model.Pit;
import com.bol.kalaha.utils.PitData;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class pitServiceImplTest {

  @InjectMocks
  private PitServiceImpl pitServiceImpl;

  @Mock
  private PitDao pitDao;

  @Test
  public void getBasePit_Ok() {
    List<Pit> pitList = PitData.mockStartPitList();
    Pit basePit = PitData.mockBasePitSouth();

    assertEquals(basePit, pitServiceImpl.getBasePit(pitList, Player.SOUTH));
  }

  @Test
  public void getBasePit_Ko_PitNotFound() {
    List<Pit> pitList = Arrays.asList(PitData.mockPitNorth(), PitData.mockBasePitNorth());

    assertThrows(PitNotFoundException.class, () -> {
      pitServiceImpl.getBasePit(pitList, Player.SOUTH);
    });
  }

  @Test
  public void getPits_Ok() {
    List<Pit> pitList = PitData.mockStartPitList();
    List<Pit> pitListPlayer = Arrays
        .asList(PitData.mockBasePitNorth(), PitData.mockPitNorth());

    assertEquals(pitListPlayer, pitServiceImpl.getPits(pitList, Player.NORTH));
  }

  @Test
  public void getPit_Ok() {
    List<Pit> pitList = PitData.mockStartPitList();
    Pit pit = PitData.mockPitSouth();

    assertEquals(pit, pitServiceImpl.getPit(pitList, pit.getId()));
  }

  @Test
  public void getPit_Ko_PitNotFound() {
    List<Pit> pitList = Arrays.asList(PitData.mockPitNorth(), PitData.mockBasePitNorth());
    int id = PitData.mockPitSouth().getId();

    assertThrows(PitNotFoundException.class, () -> {
      pitServiceImpl.getPit(pitList, id);
    });
  }

  @Test
  public void getOppositePit_Ok() {
    List<Pit> pitList = PitData.mockStartPitList();
    Pit pit = PitData.mockPitSouth();

    assertEquals(pit, pitServiceImpl.getOppositePit(pitList, PitData.mockPitNorth()));
  }

  @Test
  public void getOppositePit_Ok_BasePit() {
    List<Pit> pitList = PitData.mockStartPitList();
    Pit pit = PitData.mockBasePitSouth();

    assertEquals(pit, pitServiceImpl.getOppositePit(pitList, PitData.mockBasePitNorth()));
  }

  @Test
  public void getOppositePit_Ko_PitNotFound() {
    List<Pit> pitList = Arrays.asList(PitData.mockPitNorth(), PitData.mockBasePitNorth());
    Pit pit = PitData.mockBasePitSouth();

    assertThrows(PitNotFoundException.class, () -> {
      pitServiceImpl.getOppositePit(pitList, PitData.mockBasePitNorth());
    });
  }

  @Test
  public void newPitList_Ok() {
    when(pitDao.save(any(Pit.class)))
        .thenReturn(PitData.mockBasePitNorth());

    pitServiceImpl.newPitList(new Game());
    verify(pitDao, times(15)).save(any(Pit.class));
  }
}
