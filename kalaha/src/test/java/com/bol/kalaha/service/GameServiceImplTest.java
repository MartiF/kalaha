package com.bol.kalaha.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bol.kalaha.dao.GameDao;
import com.bol.kalaha.dao.PitDao;
import com.bol.kalaha.enums.GameState;
import com.bol.kalaha.enums.Player;
import com.bol.kalaha.exception.exceptions.BasePitMoveException;
import com.bol.kalaha.exception.exceptions.EmptyPitMoveException;
import com.bol.kalaha.exception.exceptions.GameHasEndedException;
import com.bol.kalaha.exception.exceptions.WrongTurnException;
import com.bol.kalaha.model.Game;
import com.bol.kalaha.model.Pit;
import com.bol.kalaha.model.dto.GameDto;
import com.bol.kalaha.utils.GameData;
import com.bol.kalaha.utils.PitData;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GameServiceImplTest {

  @InjectMocks
  private GameServiceImpl gameService;

  @Mock
  private GameDao gameDao;

  @Mock
  private PitDao pitDao;

  @Mock
  private PitServiceImpl pitServiceImpl;

  private int gameId = GameData.idStartGame;

  @BeforeEach
  public void setup() {
    Game game = GameData.mockStartGame();
    List<Pit> pitList = PitData.mockStartPitList();

    when(gameDao.findById(game.getId())).thenReturn(game);
    when(pitDao.findByGameId(game.getId())).thenReturn(pitList);

    doCallRealMethod().when(pitServiceImpl).getBasePit(anyList(), any(Player.class));
    doCallRealMethod().when(pitServiceImpl).getOppositePit(anyList(), any(Pit.class));
    doCallRealMethod().when(pitServiceImpl).getPit(anyList(), any(Integer.class));
    doCallRealMethod().when(pitServiceImpl).getPits(anyList(), any(Player.class));
  }

  @Test
  public void getGame_Ok() {
    Game game = GameData.mockStartGame();
    List<Pit> pitList = PitData.mockStartPitList();
    GameDto gameDto = new GameDto(game.getGameState(), game.getTurn(), pitList);

    // Mock
    when(gameDao.findById(gameId)).thenReturn(game);
    when(pitDao.findByGameId(gameId)).thenReturn(pitList);

    // Verify
    assertEquals(gameDto, gameService.getGame(gameId));
  }

  @Test
  public void getAllGames_Ok() {
    List<Game> gameList = GameData.mockGameList();

    // Mock
    when(gameDao.findAll()).thenReturn(gameList);

    // Verify
    assertEquals(gameList, gameService.getAllGames());
  }

  @Test
  public void newGame_Ok() {
    Game game = GameData.mockStartGame();

    when(gameDao.save(new Game())).thenReturn(game);
    doNothing().when(pitServiceImpl).newPitList(game);

    assertEquals(game.getId(), gameService.newGame());
    verify(pitServiceImpl, times(1)).newPitList(game);
  }

  @Test
  public void updateGame_Ok() {
    // Set board starting point
    when(pitDao.findByGameId(gameId)).thenReturn(getMockedPitList(0, 4, 0, 2));

    // Set expected board after move
    GameDto expected = getMockedGameDto(GameState.NORTH_WIN, Player.NORTH, 5, 0, 1, 0);

    // Choose south pit as move
    GameDto result = gameService.updateGame(gameId, PitData.mockPitSouth().getId());

    // Verify the board
    assertEquals(expected, result);
  }

  @Test
  public void updateGame_Ok_EndInOwnPit() {
    // Set expected board after move
    GameDto expected = getMockedGameDto(GameState.PLAYING, Player.SOUTH, 0, 5, 2, 1);

    // Choose south pit as move
    GameDto result = gameService.updateGame(gameId, PitData.mockPitSouth().getId());

    // Verify the board
    assertEquals(expected, result);
  }

  @Test
  public void updateGame_Ko_GameHasEnded() {
    Game game = new Game();
    game.setGameState(GameState.NORTH_WIN);
    when(gameDao.findById(gameId)).thenReturn(game);

    // Verify exception is thrown
    assertThrows(GameHasEndedException.class, () -> {
      gameService.updateGame(gameId, PitData.mockPitSouth().getId());
    });
  }

  @Test
  public void updateGame_Ko_BasePitMove() {
    // Verify exception is thrown
    assertThrows(BasePitMoveException.class, () -> {
      gameService.updateGame(gameId, PitData.mockBasePitSouth().getId());
    });
  }

  @Test
  public void updateGame_Ko_WrongTurn() {
    // Verify exception is thrown
    assertThrows(WrongTurnException.class, () -> {
      gameService.updateGame(gameId, PitData.mockPitNorth().getId());
    });
  }

  @Test
  public void updateGame_Ko_EmptyPitMove() {
    // Set board starting point
    when(pitDao.findByGameId(gameId)).thenReturn(getMockedPitList(0, 4, 0, 0));

    // Verify exception is thrown
    assertThrows(EmptyPitMoveException.class, () -> {
      gameService.updateGame(gameId, PitData.mockPitSouth().getId());
    });
  }

  @Test
  public void updateGame_Ok_GetBallsOppositePit() {
    // Set board starting point
    when(pitDao.findByGameId(gameId)).thenReturn(getMockedPitList(0, 3, 0, 3));

    // Set expected board after move
    GameDto expected = getMockedGameDto(GameState.SOUTH_WIN, Player.NORTH, 0, 0, 6, 0);

    // Choose south pit as move
    GameDto result = gameService.updateGame(gameId, PitData.mockPitSouth().getId());

    // Verify the board
    assertEquals(expected, result);
  }

  @Test
  public void updateGame_Ok_GameOverNorthWin() {
    // Set board starting point
    when(pitDao.findByGameId(gameId)).thenReturn(getMockedPitList(1, 1, 2, 1));

    // Set expected board after move
    GameDto expected = getMockedGameDto(GameState.SOUTH_WIN, Player.SOUTH, 2, 0, 3, 0);

    // Choose south pit as move
    GameDto result = gameService.updateGame(gameId, PitData.mockPitSouth().getId());

    // Verify the board
    assertEquals(expected, result);
  }

  @Test
  public void updateGame_Ok_GameOverSouthWin() {
    // Set board starting point
    when(pitDao.findByGameId(gameId)).thenReturn(getMockedPitList(3, 1, 1, 1));

    // Set expected board after move
    GameDto expected = getMockedGameDto(GameState.NORTH_WIN, Player.SOUTH, 4, 0, 2, 0);

    // Choose south pit as move
    GameDto result = gameService.updateGame(gameId, PitData.mockPitSouth().getId());

    // Verify the board
    assertEquals(expected, result);
  }

  @Test
  public void updateGame_Ok_GameOverDraw() {
    // Set board starting point
    when(pitDao.findByGameId(gameId)).thenReturn(getMockedPitList(1, 1, 1, 1));

    // Set expected board after move
    GameDto expected = getMockedGameDto(GameState.DRAW, Player.SOUTH, 2, 0, 2, 0);

    // Choose south pit as move
    GameDto result = gameService.updateGame(gameId, PitData.mockPitSouth().getId());

    // Verify the board
    assertEquals(expected, result);
  }

  private GameDto getMockedGameDto(GameState gameState, Player player, int northBase, int north,
      int southBase, int south) {

    return new GameDto(gameState, player, getMockedPitList(northBase, north, southBase, south));
  }

  public List<Pit> getMockedPitList(int northBase, int north, int southBase, int south) {
    Pit northBasePit = PitData.mockBasePitNorth();
    northBasePit.setBalls(northBase);

    Pit northPit = PitData.mockPitNorth();
    northPit.setBalls(north);

    Pit southBasePit = PitData.mockBasePitSouth();
    southBasePit.setBalls(southBase);

    Pit southPit = PitData.mockPitSouth();
    southPit.setBalls(south);

    return Arrays.asList(northBasePit, northPit, southBasePit, southPit);
  }
}
