package com.bol.kalaha.dao;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.bol.kalaha.utils.GameData;
import com.bol.kalaha.exception.exceptions.GameNotFoundException;
import com.bol.kalaha.model.Game;
import com.bol.kalaha.repository.GameRepository;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GameDaoTest {

  @InjectMocks
  private GameDao gameDao;

  @Mock
  private GameRepository gameRepository;

  @Test
  public void findById_Ok() {
    // Mock
    Game game = GameData.mockStartGame();
    int id = GameData.idStartGame;
    when(gameRepository.findById(id)).thenReturn(Optional.of(game));

    // Verify
    assertEquals(game, gameDao.findById(id));
  }

  @Test
  public void findById_Ko_GameNotFound() {
    // Mock
    Game game = GameData.mockStartGame();
    int id = GameData.idStartGame;
    when(gameRepository.findById(id)).thenReturn(Optional.empty());

    // Verify
    assertThrows(GameNotFoundException.class, () -> {
      gameDao.findById(id);
    });
  }

  @Test
  public void save_Ok() {
    // Mock
    Game game = GameData.mockStartGame();
    when(gameRepository.save(game)).thenReturn(game);

    // Verify
    assertEquals(game, gameDao.save(game));
  }

  @Test
  public void findAll_Ok() {
    when(gameRepository.findAll()).thenReturn(GameData.mockGameList());
    assertEquals(GameData.mockGameList(), gameRepository.findAll());
  }
}
