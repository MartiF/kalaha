package com.bol.kalaha.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import com.bol.kalaha.model.Pit;
import com.bol.kalaha.repository.PitRepository;
import com.bol.kalaha.utils.PitData;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PitDaoTest {

  @InjectMocks
  private PitDao pitDao;

  @Mock
  private PitRepository pitRepository;

  @BeforeEach
  public void setup() {
    List<Pit> pitList = PitData.mockStartPitList();
    for (Pit pit : pitList) {
      when(pitRepository.save(pit)).thenReturn(pit);
    }
  }

  @Test
  public void save_Ok() {
    Pit pit = PitData.mockPitNorth();
    assertEquals(pit, pitDao.save(pit));
  }

  @Test
  public void saveList_Ok() {
    List<Pit> pitList = PitData.mockStartPitList();
    assertEquals(pitList, pitDao.save(pitList));
    verify(pitRepository, times(pitList.size())).save(any(Pit.class));
  }

  @Test
  public void findByGameId_Ok() {
    // Mock
    List<Pit> pitList = PitData.mockStartPitList();
    int id = 123;
    when(pitRepository.findByGameId(id)).thenReturn(pitList);

    // Verify
    assertEquals(pitList, pitDao.findByGameId(id));
  }

}
