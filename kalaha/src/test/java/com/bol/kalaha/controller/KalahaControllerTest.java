package com.bol.kalaha.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bol.kalaha.exception.ExceptionInfo;
import com.bol.kalaha.exception.exceptions.BasePitMoveException;
import com.bol.kalaha.exception.exceptions.EmptyPitMoveException;
import com.bol.kalaha.exception.exceptions.GameHasEndedException;
import com.bol.kalaha.exception.exceptions.GameNotFoundException;
import com.bol.kalaha.exception.exceptions.PitNotFoundException;
import com.bol.kalaha.exception.exceptions.WrongTurnException;
import com.bol.kalaha.exception.handler.KalahaExceptionHandler;
import com.bol.kalaha.model.Game;
import com.bol.kalaha.model.dto.GameDto;
import com.bol.kalaha.service.GameService;
import com.bol.kalaha.utils.GameData;
import com.bol.kalaha.utils.GameDtoData;
import java.text.MessageFormat;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(controllers = KalahaController.class)
public class KalahaControllerTest {

  @InjectMocks
  private KalahaController kalahaController;

  @Mock
  private GameService gameService;

  private MockMvc mockMvc;

  private MockHttpServletRequestBuilder getNewGameRequest;
  private MockHttpServletRequestBuilder getAllGamesRequest;
  private MockHttpServletRequestBuilder getGameRequest;
  private MockHttpServletRequestBuilder doMoveRequest;

  private int gameId = GameData.idStartGame;
  private int moveId = 2;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.standaloneSetup(kalahaController)
        .setControllerAdvice(new KalahaExceptionHandler()).build();

    getNewGameRequest = MockMvcRequestBuilders.get("/api/v1/game/new");
    getAllGamesRequest = MockMvcRequestBuilders.get("/api/v1/game/all");
    getGameRequest = MockMvcRequestBuilders.get("/api/v1/game/" + gameId);
    doMoveRequest = MockMvcRequestBuilders.post("/api/v1/game/" + gameId + '/' + moveId);
  }

  @Test
  public void getNewGame_Ok() throws Exception {
    int id = 1;
    when(gameService.newGame()).thenReturn(id);

    // Perform GET
    ResultActions result = mockMvc.perform(getNewGameRequest);

    // Check status
    result.andExpect(status().isOk())
        // Check Id
        .andExpect(jsonPath("$").value(id));
  }

  @Test
  public void getAllGames_Ok() throws Exception {
    List<Game> gameList = GameData.mockGameList();
    when(gameService.getAllGames()).thenReturn(gameList);

    // Perform GET
    ResultActions result = mockMvc.perform(getAllGamesRequest);

    // Check status
    result.andExpect(status().isOk());

    // Check games
    for (int i = 0; i < gameList.size(); i++) {
      result.andExpect(jsonPath("$[" + i + "].id").value(gameList.get(i).getId()))
          .andExpect(
              jsonPath("$[" + i + "].gameState").value(gameList.get(i).getGameState().toString()))
          .andExpect(jsonPath("$[" + i + "].turn").value(gameList.get(i).getTurn().toString()));
    }
  }

  @Test
  public void getGame_Ok() throws Exception {
    GameDto gameDto = GameDtoData.mockGameDtoPlayingSouth();
    when(gameService.getGame(gameId)).thenReturn(gameDto);

    // Perform GET
    ResultActions result = mockMvc.perform(getGameRequest);

    // Check status
    result.andExpect(status().isOk())
        // Check gameState
        .andExpect(jsonPath("$.gameState").value(gameDto.getGameState().toString()))
        // Check turn
        .andExpect(jsonPath("$.turn").value(gameDto.getTurn().toString()));

    // Check pitList
    for (int i = 0; i < gameDto.getPitList().size(); i++) {
      result.andExpect(jsonPath("$.pitList[" + i + "]").value(gameDto.getPitList().get(i)));
    }
  }

  @Test
  public void getGame_Ko_GameNotFoundException() throws Exception {
    GameDto gameDto = GameDtoData.mockGameDtoPlayingSouth();
    String exceptionMessage = MessageFormat
        .format(ExceptionInfo.GAME_NOT_FOUND_EXCEPTION.getMessage(), String.valueOf(gameId));
    HttpStatus status = ExceptionInfo.GAME_NOT_FOUND_EXCEPTION.getHttpStatus();

    // Throw exception
    when(gameService.getGame(gameId)).thenThrow(new GameNotFoundException(String.valueOf(gameId)));

    // Perform GET
    ResultActions result = mockMvc.perform(getGameRequest);

    // Check exception status
    result.andExpect(status().is(status.value()))
        // Check status message
        .andExpect(jsonPath("$.status").value(status.name()))
        // Check exception message
        .andExpect(
            jsonPath("$.message").value(exceptionMessage));
  }

  @Test
  public void doMove_Ok() throws Exception {
    GameDto gameDto = GameDtoData.mockGameDtoPlayingSouth();
    when(gameService.updateGame(gameId, moveId)).thenReturn(gameDto);

    // Perform POST
    ResultActions result = mockMvc.perform(doMoveRequest);

    // Check status
    result.andExpect(status().isOk())
        // Check gameState
        .andExpect(jsonPath("$.gameState").value(gameDto.getGameState().toString()))
        // Check turn
        .andExpect(jsonPath("$.turn").value(gameDto.getTurn().toString()));

    // Check pitList
    for (int i = 0; i < gameDto.getPitList().size(); i++) {
      result.andExpect(jsonPath("$.pitList[" + i + "]").value(gameDto.getPitList().get(i)));
    }
  }

  @Test
  public void doMove_Ko_BasePitMoveException() throws Exception {
    GameDto gameDto = GameDtoData.mockGameDtoPlayingSouth();
    String exceptionMessage = MessageFormat
        .format(ExceptionInfo.BASE_PIT_MOVE_EXCEPTION.getMessage(), String.valueOf(gameId));
    HttpStatus status = ExceptionInfo.BASE_PIT_MOVE_EXCEPTION.getHttpStatus();

    // Throw exception
    when(gameService.updateGame(gameId, moveId))
        .thenThrow(new BasePitMoveException(String.valueOf(gameId)));

    // Perform POST
    ResultActions result = mockMvc.perform(doMoveRequest);

    // Check exception status
    result.andExpect(status().is(status.value()))
        // Check status message
        .andExpect(jsonPath("$.status").value(status.name()))
        // Check exception message
        .andExpect(
            jsonPath("$.message").value(exceptionMessage));
  }

  @Test
  public void doMove_Ko_EmptyPitMoveException() throws Exception {
    GameDto gameDto = GameDtoData.mockGameDtoPlayingSouth();
    String exceptionMessage = MessageFormat
        .format(ExceptionInfo.EMPTY_PIT_MOVE_EXCEPTION.getMessage(), String.valueOf(gameId));
    HttpStatus status = ExceptionInfo.EMPTY_PIT_MOVE_EXCEPTION.getHttpStatus();

    // Throw exception
    when(gameService.updateGame(gameId, moveId))
        .thenThrow(new EmptyPitMoveException(String.valueOf(gameId)));

    // Perform POST
    ResultActions result = mockMvc.perform(doMoveRequest);

    // Check exception status
    result.andExpect(status().is(status.value()))
        // Check status message
        .andExpect(jsonPath("$.status").value(status.name()))
        // Check exception message
        .andExpect(
            jsonPath("$.message").value(exceptionMessage));
  }

  @Test
  public void doMove_Ko_GameHasEndedException() throws Exception {
    GameDto gameDto = GameDtoData.mockGameDtoPlayingSouth();
    String exceptionMessage = MessageFormat
        .format(ExceptionInfo.GAME_HAS_ENDED_EXCEPTION.getMessage(), String.valueOf(gameId));
    HttpStatus status = ExceptionInfo.GAME_HAS_ENDED_EXCEPTION.getHttpStatus();

    // Throw exception
    when(gameService.updateGame(gameId, moveId))
        .thenThrow(new GameHasEndedException(String.valueOf(gameId)));

    // Perform POST
    ResultActions result = mockMvc.perform(doMoveRequest);

    // Check exception status
    result.andExpect(status().is(status.value()))
        // Check status message
        .andExpect(jsonPath("$.status").value(status.name()))
        // Check exception message
        .andExpect(
            jsonPath("$.message").value(exceptionMessage));
  }

  @Test
  public void doMove_Ko_PitNotFoundException() throws Exception {
    GameDto gameDto = GameDtoData.mockGameDtoPlayingSouth();
    String exceptionMessage = MessageFormat
        .format(ExceptionInfo.PIT_NOT_FOUND_EXCEPTION.getMessage(), String.valueOf(moveId));
    HttpStatus status = ExceptionInfo.PIT_NOT_FOUND_EXCEPTION.getHttpStatus();

    // Throw exception
    when(gameService.updateGame(gameId, moveId))
        .thenThrow(new PitNotFoundException(String.valueOf(moveId)));

    // Perform POST
    ResultActions result = mockMvc.perform(doMoveRequest);

    // Check exception status
    result.andExpect(status().is(status.value()))
        // Check status message
        .andExpect(jsonPath("$.status").value(status.name()))
        // Check exception message
        .andExpect(
            jsonPath("$.message").value(exceptionMessage));
  }

  @Test
  public void doMove_Ko_WrongTurnException() throws Exception {
    GameDto gameDto = GameDtoData.mockGameDtoPlayingSouth();
    String exceptionMessage = MessageFormat
        .format(ExceptionInfo.WRONG_TURN_EXCEPTION.getMessage(), String.valueOf(gameId));
    HttpStatus status = ExceptionInfo.WRONG_TURN_EXCEPTION.getHttpStatus();

    // Throw exception
    when(gameService.updateGame(gameId, moveId))
        .thenThrow(new WrongTurnException(String.valueOf(gameId)));

    // Perform POST
    ResultActions result = mockMvc.perform(doMoveRequest);

    // Check exception status
    result.andExpect(status().is(status.value()))
        // Check status message
        .andExpect(jsonPath("$.status").value(status.name()))
        // Check exception message
        .andExpect(
            jsonPath("$.message").value(exceptionMessage));
  }

}

