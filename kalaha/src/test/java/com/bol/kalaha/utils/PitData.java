package com.bol.kalaha.utils;

import com.bol.kalaha.enums.Player;
import com.bol.kalaha.model.Game;
import com.bol.kalaha.model.Pit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PitData {

  public static Game game = new Game();

  public static Pit mockBasePitSouth() {
    Pit pit = new Pit(game, 2, Player.SOUTH, 0, 7, true);
    pit.setId(3);
    return pit;
  }

  public static Pit mockPitSouth() {
    Pit pit = new Pit(game, 3, Player.SOUTH, 4, 0, false);
    pit.setId(4);
    return pit;
  }

  public static Pit mockBasePitNorth() {
    Pit pit = new Pit(game, 4, Player.NORTH, 0, 7, true);
    pit.setId(1);
    return pit;
  }

  public static Pit mockPitNorth() {
    Pit pit = new Pit(game, 1, Player.NORTH, 4, 5, false);
    pit.setId(2);
    return pit;
  }

  public static List<Pit> mockStartPitList() {
    return Arrays.asList(mockBasePitNorth(), mockPitNorth(), mockBasePitSouth(), mockPitSouth());
  }
}
