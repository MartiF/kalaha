package com.bol.kalaha.utils;

import com.bol.kalaha.enums.GameState;
import com.bol.kalaha.model.Game;
import java.util.Arrays;
import java.util.List;

public class GameData {

  public static final int idStartGame = 12345;
  public static final int idNorthWinGame = 1357;

  public static Game mockStartGame() {
    Game game = new Game();
    game.setId(idStartGame);
    return game;
  }

  public static Game mockNorthWinGame() {
    Game game = new Game();
    game.setId(idNorthWinGame);
    game.setGameState(GameState.NORTH_WIN);
    return game;
  }

  public static List<Game> mockGameList() {
    return Arrays.asList(mockStartGame(), mockNorthWinGame());
  }

}
