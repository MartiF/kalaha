package com.bol.kalaha.utils;

import com.bol.kalaha.enums.GameState;
import com.bol.kalaha.enums.Player;
import com.bol.kalaha.model.Pit;
import com.bol.kalaha.model.dto.GameDto;
import java.util.List;

public class GameDtoData {

  public static GameDto mockGameDtoPlayingSouth() {
    return new GameDto(GameState.PLAYING, Player.SOUTH, mockPitListForGameDto());
  }

  private static List<Pit> mockPitListForGameDto() {
    List<Pit> pitList = PitData.mockStartPitList();
    for (Pit pit : pitList) {
      pit.setGame(null);
      pit.setIdNextPit(0);
    }
    return pitList;
  }
}
