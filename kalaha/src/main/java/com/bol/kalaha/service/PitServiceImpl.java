package com.bol.kalaha.service;

import com.bol.kalaha.dao.PitDao;
import com.bol.kalaha.enums.Player;
import com.bol.kalaha.exception.exceptions.PitNotFoundException;
import com.bol.kalaha.model.Game;
import com.bol.kalaha.model.Pit;
import com.bol.kalaha.util.GameConstants;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PitServiceImpl implements PitService {

  @Autowired
  private PitDao pitDao;

  /**
   * @param pitList Pit list of the current Game
   * @param player  Player you want the base Pit from
   * @return
   */
  @Override
  public Pit getBasePit(List<Pit> pitList, Player player) {
    return pitList.stream().filter(Pit::getIsBasePit)
        .filter(p -> p.getPlayer() == player).findFirst()
        .orElseThrow(() -> new PitNotFoundException("No known id"));
  }

  /**
   * @param pitList Pit list of the current Game
   * @param player  Player you want the Pits from
   * @return List of pits of the given player
   */
  @Override
  public List<Pit> getPits(List<Pit> pitList, Player player) {
    return pitList.stream().filter(p -> p.getPlayer() == player).collect(Collectors.toList());
  }

  /**
   * Gets a Pit based on id
   *
   * @param pitList Pit list of the current Game
   * @param id      Id of the pit you are looking for
   * @return Pit
   */
  @Override
  public Pit getPit(List<Pit> pitList, int id) {
    return pitList.stream().filter(p -> p.getId() == id).findFirst()
        .orElseThrow(() -> new PitNotFoundException(String.valueOf(id)));
  }

  /**
   * Gets the opposing pit
   *
   * @param pitList Pit list of the current Game
   * @param pit     Starting Pit
   * @return Pit
   */
  @Override
  public Pit getOppositePit(List<Pit> pitList, Pit pit) {
    // If base pit
    if (pit.getIsBasePit()) {
      return getBasePit(pitList, pit.getPlayer().getOtherPlayer());
    }

    // if not base pit
    return pitList.stream()
        .filter(p -> p.getPlayer() == pit.getPlayer().getOtherPlayer())
        .filter(p -> p.getPitNumber() == (GameConstants.boardSize - 1) - pit.getPitNumber())
        .findFirst().orElseThrow(() -> new PitNotFoundException("No known id"));
  }

  /**
   * Sets up a Pit list for a new game
   *
   * @param game game to set up the Pit list for
   */
  @Override
  public void newPitList(Game game) {
    // Set base pit north
    Pit basePitNorth = pitDao
        .save(new Pit(game, 0, Player.NORTH, 0, GameConstants.boardSize, true));
    int currentPit = basePitNorth.getId();

    // Set pits south
    for (int i = GameConstants.boardSize - 1; i >= 0; i--) {
      currentPit = pitDao
          .save(new Pit(game, currentPit, Player.NORTH, GameConstants.nrOfBalls, i, false)).getId();
    }

    // Set base pit south
    currentPit = pitDao
        .save(new Pit(game, currentPit, Player.SOUTH, 0, GameConstants.boardSize, true)).getId();

    // Set pits north
    for (int i = GameConstants.boardSize - 1; i >= 0; i--) {
      currentPit = pitDao
          .save(new Pit(game, currentPit, Player.SOUTH, GameConstants.nrOfBalls, i, false)).getId();
    }

    // set correct next pit for base pit north
    basePitNorth.setIdNextPit(currentPit);
    pitDao.save(basePitNorth);
  }
}
