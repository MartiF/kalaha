package com.bol.kalaha.service;

import com.bol.kalaha.enums.Player;
import com.bol.kalaha.model.Game;
import com.bol.kalaha.model.Pit;
import java.util.List;

public interface PitService {

  /**
   * Get the base pit of the given player
   *
   * @param pitList Pit list of the current Game
   * @param player  Player you want the base Pit from
   * @return base Pit
   */
  Pit getBasePit(List<Pit> pitList, Player player);

  /**
   * Get all the pits of a player
   *
   * @param pitList Pit list of the current Game
   * @param player  Player you want the Pits from
   * @return List of Pits
   */
  List<Pit> getPits(List<Pit> pitList, Player player);

  /**
   * Get a Pit with the given id
   *
   * @param pitList Pit list of the current Game
   * @param id      Id of the pit you are looking for
   * @return Pit
   */
  Pit getPit(List<Pit> pitList, int id);

  /**
   * Gets the opposing pit
   *
   * @param pitList Pit list of the current Game
   * @param pit     Starting Pit
   * @return Pit
   */
  Pit getOppositePit(List<Pit> pitList, Pit pit);

  /**
   * Sets up a Pit list for a new game
   *
   * @param game game to set up the Pit list for
   */
  void newPitList(Game game);
}
