package com.bol.kalaha.service;

import com.bol.kalaha.dao.GameDao;
import com.bol.kalaha.dao.PitDao;
import com.bol.kalaha.enums.GameState;
import com.bol.kalaha.enums.Player;
import com.bol.kalaha.exception.exceptions.BasePitMoveException;
import com.bol.kalaha.exception.exceptions.EmptyPitMoveException;
import com.bol.kalaha.exception.exceptions.GameHasEndedException;
import com.bol.kalaha.exception.exceptions.WrongTurnException;
import com.bol.kalaha.model.Game;
import com.bol.kalaha.model.Pit;
import com.bol.kalaha.model.dto.GameDto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameServiceImpl implements GameService {

  @Autowired
  private PitService pitServiceImpl;

  @Autowired
  private GameDao gameDao;

  @Autowired
  private PitDao pitDao;

  /**
   * Gets a game based on the id
   *
   * @param id the game id
   * @return GameDto
   */
  @Override
  public GameDto getGame(int id) {
    Game game = gameDao.findById(id);
    List<Pit> pitList = pitDao.findByGameId(game.getId());

    return new GameDto(game.getGameState(), game.getTurn(), pitList);
  }

  /**
   * Get all games
   *
   * @return list of all games
   */
  @Override
  public List<Game> getAllGames() {
    return gameDao.findAll();
  }

  /**
   * Update the game with the move the player made
   *
   * @param id   the Game id
   * @param move the id of the Pit the player chose
   * @return GameDto with the new state of the game
   */
  @Override
  public GameDto updateGame(int id, int move) {
    Game game = gameDao.findById(id);
    List<Pit> pitList = pitDao.findByGameId(game.getId());

    // Validate if the move is legal
    checkMove(pitList, move, game);

    // Divide the balls from the chosen pit
    Pit endPit = handleMove(pitList, move);

    // Change the turn
    changeTurns(game, endPit);

    // handle game over if the game is over
    handleGameOver(game, pitList);

    // Save game and pits to db
    gameDao.save(game);
    pitDao.save(pitList);

    return new GameDto(game.getGameState(), game.getTurn(), pitList);
  }

  /**
   * Starts a new game
   *
   * @return id of the new game
   */
  @Override
  public int newGame() {
    // New game
    Game game = gameDao.save(new Game());

    // Set Pit list
    pitServiceImpl.newPitList(game);

    // return the new game id
    return game.getId();
  }

  /**
   * Handles a move from a player
   *
   * @param pitList Pit list of the current Game
   * @param move    Chosen pit id of current player
   * @return The pit the turn ends in
   */
  private Pit handleMove(List<Pit> pitList, int move) {
    // Get balls
    Pit pit = pitServiceImpl.getPit(pitList, move);
    int balls = pit.getBalls();
    Player currentPlayer = pit.getPlayer();
    pit.setBalls(0);

    // Divide balls
    for (int i = balls; i > 0; i--) {
      pit = pitServiceImpl.getPit(pitList, pit.getIdNextPit());

      // only add a ball if its not the opponent base pit
      if (pit.getPlayer() == currentPlayer.getOtherPlayer() && pit.getIsBasePit()) {
        i++;
      } else {
        pit.addBalls(1);
      }
    }

    // If the player ends in his own empty pit:
    // Take the balls from that pit and the opposite pit, and put it in the players base pit
    if (pit.getBalls() == 1 && pit.getPlayer() == currentPlayer && !pit.getIsBasePit()) {
      Pit oppositePit = pitServiceImpl.getOppositePit(pitList, pit);
      Pit basePit = pitServiceImpl.getBasePit(pitList, pit.getPlayer());

      // Add balls to the base pit
      basePit.addBalls(oppositePit.getBalls() + pit.getBalls());

      // Remove the balls from the 2 pits
      pit.setBalls(0);
      oppositePit.setBalls(0);
    }

    // return end pit
    return pit;
  }

  /**
   * Changes turns if the player who was playing did not end in his own base pit
   *
   * @param game current game
   * @param pit  the pit the turn ended in
   */
  private void changeTurns(Game game, Pit pit) {
    if (!(game.getTurn() == pit.getPlayer() && pit.getIsBasePit())) {
      game.setTurn(game.getTurn().getOtherPlayer());
    }
  }

  /**
   * Checks if the move the player wants to make is legal
   *
   * @param pitList Pit list of the current Game
   * @param move    Id of the Pit chosen
   * @param game    Current game
   */
  private void checkMove(List<Pit> pitList, int move, Game game) {
    // Check if the game has not ended
    if (game.getGameState() != GameState.PLAYING) {
      throw new GameHasEndedException(String.valueOf(game.getId()));
    }

    // Check if pit exists in pit list
    Pit pit = pitServiceImpl.getPit(pitList, move);

    // Check if it is not a base pit
    if (pit.getIsBasePit()) {
      throw new BasePitMoveException(String.valueOf(move));
    }

    // Check if it is that player's turn
    if (pit.getPlayer() != game.getTurn()) {
      throw new WrongTurnException(String.valueOf(game.getTurn()));
    }

    // Check if it is not empty
    if (pit.getBalls() == 0) {
      throw new EmptyPitMoveException(String.valueOf(game.getTurn()));
    }
  }

  /**
   * When one sides is out of balls, handles the game over procedure
   *
   * @param game    current Game
   * @param pitList Pit list for current Game
   */
  private void handleGameOver(Game game, List<Pit> pitList) {
    // Check if one side is out of balls
    int totalBallsNorth = pitServiceImpl.getPits(pitList, Player.NORTH).stream()
        .filter(p -> !p.getIsBasePit())
        .mapToInt(Pit::getBalls).sum();
    int totalBallsSouth = pitServiceImpl.getPits(pitList, Player.SOUTH).stream()
        .filter(p -> !p.getIsBasePit())
        .mapToInt(Pit::getBalls).sum();

    // Add the remaining balls to the base pit
    if (totalBallsNorth == 0) {
      pitServiceImpl.getBasePit(pitList, Player.SOUTH).addBalls(totalBallsSouth);
      pitServiceImpl.getPits(pitList, Player.SOUTH).stream().filter(p -> !p.getIsBasePit())
          .forEach(p -> p.setBalls(0));
    } else if (totalBallsSouth == 0) {
      pitServiceImpl.getBasePit(pitList, Player.NORTH).addBalls(totalBallsNorth);
      pitServiceImpl.getPits(pitList, Player.NORTH).stream().filter(p -> !p.getIsBasePit())
          .forEach(p -> p.setBalls(0));
    }

    // Change the gameState
    if (totalBallsNorth == 0 || totalBallsSouth == 0) {
      int finalBallsNorth = pitServiceImpl.getBasePit(pitList, Player.NORTH).getBalls();
      int finalBallsSouth = pitServiceImpl.getBasePit(pitList, Player.SOUTH).getBalls();

      if (finalBallsNorth > finalBallsSouth) {
        game.setGameState(GameState.NORTH_WIN);
      } else if (finalBallsNorth < finalBallsSouth) {
        game.setGameState(GameState.SOUTH_WIN);
      } else {
        game.setGameState(GameState.DRAW);
      }
    }
  }
}
