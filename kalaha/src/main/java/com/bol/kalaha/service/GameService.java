package com.bol.kalaha.service;

import com.bol.kalaha.model.Game;
import com.bol.kalaha.model.dto.GameDto;
import java.util.List;

public interface GameService {

  /**
   * Get a game with the current game id
   *
   * @param id the game id
   * @return GameDto
   */
  GameDto getGame(int id);

  /**
   * Get all games
   *
   * @return list of all games
   */
  List<Game> getAllGames();

  /**
   * Update the game with the move the player made
   *
   * @param id     the Game id
   * @param move   the id of the Pit the player chose
   * @return GameDto with the new state of the game
   */
  GameDto updateGame(int id, int move);

  /**
   * Starts a new game
   *
   * @return id of the game
   */
  int newGame();
}
