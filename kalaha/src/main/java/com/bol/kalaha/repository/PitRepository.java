package com.bol.kalaha.repository;

import com.bol.kalaha.model.Pit;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PitRepository extends JpaRepository<Pit, Integer> {
  List<Pit> findByGameId(int gameId);
}
