package com.bol.kalaha.controller;

import com.bol.kalaha.model.Game;
import com.bol.kalaha.model.dto.GameDto;
import com.bol.kalaha.service.GameService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for the Kalaha Game
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1/game")
public class KalahaController {

  @Autowired
  private GameService gameServiceImpl;

  /**
   * Creates a new game
   *
   * @return id of the new game
   */
  @GetMapping(value = "/new", produces = MediaType.APPLICATION_JSON_VALUE)
  public int getNewGame() {
    return gameServiceImpl.newGame();
  }

  /**
   * Gets all meta data all running games
   *
   * @return List of Games
   */
  @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
  public List<Game> getAllGames() {
    return gameServiceImpl.getAllGames();
  }

  /**
   * Gets a game
   *
   * @param gameId id of the game
   * @return GameDto -> game + list of pits
   */
  @GetMapping(value = "/{game_id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public GameDto getGame(@PathVariable(name = "game_id") final int gameId) {
    return gameServiceImpl.getGame(gameId);
  }

  /**
   * Makes a move for a game
   *
   * @param gameId id of the game
   * @param move   id of the chosen pit
   * @return GameDto with updated data
   */
  @PostMapping(value = "/{game_id}/{move}",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public GameDto doMove(@PathVariable(name = "game_id") final int gameId,
      @PathVariable(name = "move") final int move) {
    return gameServiceImpl.updateGame(gameId, move);
  }

}
