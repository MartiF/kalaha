package com.bol.kalaha.model.dto;

import com.bol.kalaha.enums.GameState;
import com.bol.kalaha.enums.Player;
import com.bol.kalaha.model.Pit;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Game Data Transfer Object, used to transfer the game state to the front end
 */
@Data
@AllArgsConstructor
public class GameDto {

  private GameState gameState;

  private Player turn;

  private List<Pit> pitList;
}
