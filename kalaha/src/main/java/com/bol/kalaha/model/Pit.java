package com.bol.kalaha.model;

import com.bol.kalaha.enums.Player;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * Entity for Pit
 */
@Data
@Entity
@Table(name = "pits")
@NoArgsConstructor
public class Pit {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "game_id", nullable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)
  @JsonIgnore
  private Game game;

  @JsonIgnore
  private int idNextPit;

  private int balls;

  // Used for the position on the board
  private int pitNumber;

  private Player player;

  private Boolean isBasePit;

  public Pit(Game game, int idNextPit, Player player, int balls, int pitNumber, boolean isBasePit) {
    this.game = game;
    this.idNextPit = idNextPit;
    this.player = player;
    this.pitNumber = pitNumber;
    this.balls = balls;
    this.isBasePit = isBasePit;
  }

  public void addBalls(int balls) {
    this.balls += balls;
  }
}
