package com.bol.kalaha.model;

import com.bol.kalaha.enums.GameState;
import com.bol.kalaha.enums.Player;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 * Entity for game, it has game meta data
 */
@Data
@Entity
@Table(name = "games")
public class Game {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private GameState gameState;

  private Player turn;

  public Game() {
    this.gameState = GameState.PLAYING;
    this.turn = Player.SOUTH;
  }
}
