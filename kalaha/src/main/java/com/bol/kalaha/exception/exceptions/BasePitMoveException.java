package com.bol.kalaha.exception.exceptions;

public class BasePitMoveException extends KalahaException {
  public BasePitMoveException(String parameter) {
    super(parameter);
  }
}
