package com.bol.kalaha.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ExceptionInfo {
  BASE_PIT_MOVE_EXCEPTION(HttpStatus.BAD_REQUEST,"You can not select a base pit for a move. Selected pit: {0}"),
  EMPTY_PIT_MOVE_EXCEPTION(HttpStatus.BAD_REQUEST,"This Pit is empty and can not be played! Selected pit: {0}"),
  GAME_HAS_ENDED_EXCEPTION(HttpStatus.BAD_REQUEST, "This Game has already ended: {0}"),
  GAME_NOT_FOUND_EXCEPTION(HttpStatus.NOT_FOUND, "Can not find Game with id: {0}"),
  PIT_NOT_FOUND_EXCEPTION(HttpStatus.NOT_FOUND, "Can not find Pit with id: {0}"),
  WRONG_TURN_EXCEPTION(HttpStatus.BAD_REQUEST, "Not your turn. {0} is next to make a move.");

  private final HttpStatus httpStatus;
  private final String message;

  /**
   * Has all the information of the custom error messaging
   *
   * @param httpStatus http status to return
   * @param message    message to return
   */
  ExceptionInfo(HttpStatus httpStatus, String message) {
    this.httpStatus = httpStatus;
    this.message = message;
  }
}
