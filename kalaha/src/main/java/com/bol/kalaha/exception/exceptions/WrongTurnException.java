package com.bol.kalaha.exception.exceptions;

public class WrongTurnException extends KalahaException {
  public WrongTurnException(String parameter) {
    super(parameter);
  }
}
