package com.bol.kalaha.exception.handler;

import com.bol.kalaha.exception.ApiError;
import com.bol.kalaha.exception.ExceptionInfo;
import com.bol.kalaha.exception.exceptions.BasePitMoveException;
import com.bol.kalaha.exception.exceptions.EmptyPitMoveException;
import com.bol.kalaha.exception.exceptions.GameHasEndedException;
import com.bol.kalaha.exception.exceptions.GameNotFoundException;
import com.bol.kalaha.exception.exceptions.PitNotFoundException;
import com.bol.kalaha.exception.exceptions.WrongTurnException;
import java.text.MessageFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class KalahaExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler({WrongTurnException.class})
  public ResponseEntity<Object> handleWrongTurnException(WrongTurnException ex, WebRequest request) {
    String error = MessageFormat.format(ExceptionInfo.WRONG_TURN_EXCEPTION.getMessage(), ex.getParameter());
    ApiError apiError =
        new ApiError(ExceptionInfo.WRONG_TURN_EXCEPTION.getHttpStatus(), error);
    return new ResponseEntity<Object>(
        apiError, new HttpHeaders(), apiError.getStatus());
  }

  @ExceptionHandler({BasePitMoveException.class})
  public ResponseEntity<Object> handleBasePitMoveException(BasePitMoveException ex, WebRequest request) {
    String error = MessageFormat.format(ExceptionInfo.BASE_PIT_MOVE_EXCEPTION.getMessage(), ex.getParameter());
    ApiError apiError =
        new ApiError(ExceptionInfo.BASE_PIT_MOVE_EXCEPTION.getHttpStatus(), error);
    return new ResponseEntity<Object>(
        apiError, new HttpHeaders(), apiError.getStatus());
  }

  @ExceptionHandler({EmptyPitMoveException.class})
  public ResponseEntity<Object> handleEmptyPitMoveException(EmptyPitMoveException ex, WebRequest request) {
    String error = MessageFormat.format(ExceptionInfo.EMPTY_PIT_MOVE_EXCEPTION.getMessage(), ex.getParameter());
    ApiError apiError =
        new ApiError(ExceptionInfo.EMPTY_PIT_MOVE_EXCEPTION.getHttpStatus(), error);
    return new ResponseEntity<Object>(
        apiError, new HttpHeaders(), apiError.getStatus());
  }

  @ExceptionHandler({GameHasEndedException.class})
  public ResponseEntity<Object> handleGameHasEndedException(GameHasEndedException ex, WebRequest request) {
    String error = MessageFormat.format(ExceptionInfo.GAME_HAS_ENDED_EXCEPTION.getMessage(), ex.getParameter());
    ApiError apiError =
        new ApiError(ExceptionInfo.GAME_HAS_ENDED_EXCEPTION.getHttpStatus(), error);
    return new ResponseEntity<Object>(
        apiError, new HttpHeaders(), apiError.getStatus());
  }

  @ExceptionHandler({PitNotFoundException.class})
  public ResponseEntity<Object> handlePitNotFoundException(PitNotFoundException ex, WebRequest request) {
    String error = MessageFormat.format(ExceptionInfo.PIT_NOT_FOUND_EXCEPTION.getMessage(), ex.getParameter());
    ApiError apiError =
        new ApiError(ExceptionInfo.PIT_NOT_FOUND_EXCEPTION.getHttpStatus(), error);
    return new ResponseEntity<Object>(
        apiError, new HttpHeaders(), apiError.getStatus());
  }

  @ExceptionHandler({GameNotFoundException.class})
  public ResponseEntity<Object> handleGameNotFoundException(GameNotFoundException ex, WebRequest request) {
    String error = MessageFormat.format(ExceptionInfo.GAME_NOT_FOUND_EXCEPTION.getMessage(), ex.getParameter());
    ApiError apiError =
        new ApiError(ExceptionInfo.GAME_NOT_FOUND_EXCEPTION.getHttpStatus(), error);
    return new ResponseEntity<Object>(
        apiError, new HttpHeaders(), apiError.getStatus());
  }
}
