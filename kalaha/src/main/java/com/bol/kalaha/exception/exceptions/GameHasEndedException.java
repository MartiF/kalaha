package com.bol.kalaha.exception.exceptions;

public class GameHasEndedException extends KalahaException {
  public GameHasEndedException(String parameter) {
    super(parameter);
  }
}
