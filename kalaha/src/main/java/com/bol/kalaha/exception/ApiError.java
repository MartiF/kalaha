package com.bol.kalaha.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * Used as exception message
 */
@Getter
public class ApiError {

  private HttpStatus status;
  private String message;

  public ApiError(HttpStatus status, String message) {
    super();
    this.status = status;
    this.message = message;
  }
}
