package com.bol.kalaha.exception.exceptions;

public class EmptyPitMoveException extends KalahaException {
  public EmptyPitMoveException(String parameter) {
    super(parameter);
  }
}
