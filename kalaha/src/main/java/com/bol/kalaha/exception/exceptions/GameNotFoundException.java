package com.bol.kalaha.exception.exceptions;

public class GameNotFoundException extends KalahaException {
  public GameNotFoundException(String parameter) {
    super(parameter);
  }
}
