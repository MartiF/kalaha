package com.bol.kalaha.exception.exceptions;

public class PitNotFoundException extends KalahaException {
  public PitNotFoundException(String parameter) {
    super(parameter);
  }
}
