package com.bol.kalaha.exception.exceptions;

import lombok.Getter;

@Getter
public class KalahaException extends RuntimeException {
  private String parameter;

  private Exception e;

  public KalahaException(String parameter) {
    this.parameter = parameter;
  }

  public KalahaException(String parameter, Exception e) {
    this.parameter = parameter;
    this.e = e;
  }
}
