package com.bol.kalaha.dao;

import com.bol.kalaha.exception.exceptions.GameNotFoundException;
import com.bol.kalaha.model.Game;
import com.bol.kalaha.repository.GameRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GameDao {

  @Autowired
  private GameRepository gameRepository;

  /**
   * Finds Game by id
   *
   * @param id game id
   * @return Game
   */
  public Game findById(int id) {
    Optional<Game> opt = gameRepository.findById(id);
    if (opt.isPresent()) {
      return opt.get();
    }

    throw new GameNotFoundException(String.valueOf(id));
  }

  /**
   * Saves a game to the db
   *
   * @param game game to save
   * @return saved game
   */
  public Game save(Game game) {
    return gameRepository.save(game);
  }

  /**
   * Finds all games
   *
   * @return List of games
   */
  public List<Game> findAll() {
    return gameRepository.findAll();
  }
}
