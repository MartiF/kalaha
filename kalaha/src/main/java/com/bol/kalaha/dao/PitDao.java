package com.bol.kalaha.dao;

import com.bol.kalaha.model.Pit;
import com.bol.kalaha.repository.PitRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PitDao {

  @Autowired
  private PitRepository pitRepository;

  /**
   * Save a pit
   *
   * @param pit Pit to save
   * @return Saved Pit
   */
  public Pit save(Pit pit) {
    return pitRepository.save(pit);
  }

  /**
   * Save a list of pits
   *
   * @param pitList Pits to save
   * @return Saved list of Pits
   */
  public List<Pit> save(List<Pit> pitList) {
    for (Pit pit : pitList) {
      pit = save(pit);
    }
    return pitList;
  }

  /**
   * Find all Pits with a given game id
   *
   * @param gameId game id to search for
   * @return List of Pits with the given id
   */
  public List<Pit> findByGameId(int gameId) {
    return pitRepository.findByGameId(gameId);
  }
}
