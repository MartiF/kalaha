package com.bol.kalaha.enums;

/**
 * The possible game states
 */
public enum GameState {
  PLAYING,
  NORTH_WIN,
  SOUTH_WIN,
  DRAW
}
