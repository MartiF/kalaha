package com.bol.kalaha.enums;

import lombok.Getter;

/**
 * The 2 players of the game
 */
@Getter
public enum Player {
  NORTH,
  SOUTH;

  static {
    (SOUTH.otherPlayer = NORTH).otherPlayer = SOUTH;
  }

  /**
   * Used to get the other player, without knowing which player it is
   */
  private Player otherPlayer;
}
