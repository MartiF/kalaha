import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/api.service';
import { GameDto } from '../models/gameDto';

@Component({
  selector: 'app-kalaha',
  templateUrl: './kalaha.component.html',
  styleUrls: ['./kalaha.component.css']
})
export class KalahaComponent implements OnInit {

  gameDto: GameDto = null;
  headers: any;
  id: String = "";
  isSubmitted = false;

  constructor(private api: ApiService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get("id");
    })

    this.getGameDto(this.id);
  }

  getGameDto(id: String) {
    this.api.getGameDto(id)
      .subscribe(resp => {
        const keys = resp.headers.keys();
        this.headers = keys.map(key =>
          `${key}: ${resp.headers.get(key)}`);

        this.gameDto = resp.body
      });
  }

  postGameDto(id: String, move: String) {
    this.api.postMove(id, move)
      .subscribe(resp => {
        const keys = resp.headers.keys();
        this.headers = keys.map(key =>
          `${key}: ${resp.headers.get(key)}`);

        this.gameDto = resp.body
      });
  }


  /*########### Template Driven Form ###########*/
  submitForm(form: NgForm) {
    this.isSubmitted = true;
    if (!form.valid) {
      return false;
    } else {
      this.postGameDto(this.id, form.value.pits);
    }
  }
}
