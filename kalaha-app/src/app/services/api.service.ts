import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Game } from '.././models/game';
import { Observable } from 'rxjs';
import { GameDto } from '../models/gameDto';

const apiUrl = 'http://localhost:8080/api/v1/game/';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getNewGame(): Observable<HttpResponse<String>> {
    return this.http.get<String>(
      apiUrl + 'new', { observe: 'response' });
  }

  getAllGames(): Observable<HttpResponse<Game[]>> {
    return this.http.get<Game[]>(
      apiUrl + 'all', { observe: 'response' });
  }

  getGameDto(id: String): Observable<HttpResponse<GameDto>> {
    return this.http.get<GameDto>(
      apiUrl + id, { observe: 'response' });
  }

  postMove(id: String, move: String): Observable<HttpResponse<GameDto>> {
    return this.http.post<GameDto>(apiUrl + id + '/' + move, null, { observe: 'response' });
  }
}
