import { Pit } from './pit';

export interface GameDto {
  gameState: string;
  turn: string;
  pitList: Pit[];
}