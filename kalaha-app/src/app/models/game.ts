export interface Game {
    id: number;
    turn: string;
    gameState: string;
  }