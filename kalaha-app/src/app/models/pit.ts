export interface Pit {
  id: number;
  balls: number;
  pitNumber: number;
  player: string;
  isBasePit: boolean;
}