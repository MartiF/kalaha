import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Game } from '../models/game';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  games: Game[] = [];
  headers: any;
  newGameId: String;

  constructor(private api: ApiService, private router: Router) {}

  ngOnInit() {
    this.getAllGames();
  }

  goToGame(id: String){
    this.router.navigate(['/' + id]);
  }

  goToNewGame() {
    this.api.getNewGame()
    .subscribe(resp => {
      this.router.navigate(['/' + resp.body]);
    });    
  }

  getAllGames() {
    this.api.getAllGames()
    .subscribe(resp => {
      const keys = resp.headers.keys();
      this.headers = keys.map(key =>
        `${key}: ${resp.headers.get(key)}`);
  
      for (const data of resp.body) {
        this.games.push(data);
      }
    });
  }

}
