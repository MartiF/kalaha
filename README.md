# README #

This repository holds an implementation of the Kalaha Game.
Used as an assessment for bol.

Inside there is a java spring boot application with a h2 database that holds the business logic.

Next to that there is an Angular app, just to quickly show the game in action.

# Getting started
To get the application running we need to start up both the front and the back-end

### Java Spring Boot - back end
In the kalaha/target folder run the following comands:

`mvnw clean install` to install.

`mvnw spring-boot:run` to run the application

To check if it works, checkout the api documentation on `http://localhost:8080/swagger-ui.html`

You can also run unit tests with `mvnw clean test`

### Angular app - front end
In the kalaha-app folder, run `npm install`. (make sure you have npm installed!)

After everything is installed run `npm start` to start up the front-end. 

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Owner ###

Made by Marti Fagel